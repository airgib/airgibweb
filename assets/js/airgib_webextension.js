jQuery(document).ready(function($) {
	if ($('ul.stickem').length) {
		var sidemenu = $('ul.stickem').offset().top;
		$(window).on('scroll', function(e) {
			var top = $(window).scrollTop(),
				elem = $('ul.stickem'),
				buffer = 105,
				width = elem.width();
			if (top > sidemenu - buffer) {
				if (!elem.hasClass('affix')) {
					elem.addClass('affix');
					elem.css('position', 'fixed');
					elem.width(width);
					elem.css('top', buffer);
				}
				//elem.width(width);
			} else {
				elem.removeClass('affix');
				elem.css('position', 'relative');
				elem.css('top', 0);
			}
		});
	}
	$('#result').on('click', function(ev) {
		calculateDragDrop();
		$('#assessment_form').submit();
		$(this).find('.elementor-button-text').html('<i class="fa fa-spin fa-gear"></i>');
		ev.preventDefault();
		ev.stopPropagation();
		return false;
	});
	$('#next').on('click', function(ev) {
		var count = $('#assessment_form').data('count'),
			total = $('#assessment_form').data('total');


		$('[data-count="' + count + '"]').not('form').hide();

		count++;

		//check if dependent
		var dependent = $('fieldset[data-count="' + count + '"]').not('form').data('dependent');
		if (dependent) {
			var field = $('fieldset[data-count="' + (count - 1) + '"]').not('form'),
				selectedValue = field.find('input:checked').val();

			//console.log('selectedValue', selectedValue);
			$('[data-count="' + count + '"][data-dependent="' + selectedValue + '"]').not('form').hide();
			$('[data-count="' + count + '"][data-dependent="' + selectedValue + '"]').not('form').removeClass('hidden');
			$('[data-count="' + count + '"][data-dependent="' + selectedValue + '"]').not('form').fadeIn();

			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="text"]').val('');
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="text"]').closest('label').removeClass('disabled');

			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').attr('checked', false);
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').removeAttr('disabled');
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').closest('label').removeClass('disabled');

		} else {
			$('[data-count="' + count + '"]').not('form').hide();
			$('[data-count="' + count + '"]').not('form').removeClass('hidden');
			$('[data-count="' + count + '"]').not('form').fadeIn();
		}

		$('.counter span').html(count);
		$('.counter h1').html(count);

		if (count > 1) {
			$('#prev').removeClass('hidden');
		} else $('#prev').addClass('hidden');

		if (count == total) {
			$('#next').addClass('hidden');
			$('#result').hide();
			$('#result').removeClass('hidden');
			$('#result').fadeIn();
		} else {
			$('#next').removeClass('hidden');
			$('#result').hide();
		}

		$('#assessment_form').data('count', count);
		//console.log('next');
		return false;
	});

	$('#prev').on('click', function(ev) {
		var count = $('#assessment_form').data('count'),
			total = $('#assessment_form').data('total');

		$('[data-count="' + count + '"]').not('form').hide();

		count--;

		//check if dependent
		var dependent = $('fieldset[data-count="' + count + '"]').not('form').data('dependent');
		//console.log('dependent', dependent);
		if (dependent) {
			var field = $('fieldset[data-count="' + (count - 1) + '"]').not('form'),
				selectedValue = field.find('input:checked').val();

			console.log('selectedValue', selectedValue);
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="text"]').val('');
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="text"]').closest('label').removeClass('disabled');

			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').attr('checked', false);
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').removeAttr('disabled');
			$('[data-count="' + count + '"]').not('[data-dependent="' + selectedValue + '"]').find('input[type="checkbox"]').closest('label').removeClass('disabled');

		} else {
			$('[data-count="' + count + '"]').not('form').hide();
			$('[data-count="' + count + '"]').not('form').removeClass('hidden');
			$('[data-count="' + count + '"]').not('form').fadeIn();
		}


		$('.counter span').html(count);
		$('.counter h1').html(count);

		if (count == 1) {
			$(this).addClass('hidden');
		}
		if (count < total) {
			$('#next').removeClass('hidden');
			$('#result').hide();
		}
		$('#assessment_form').data('count', count);
		//console.log('prev');
		return false;
	});
	var calculateDragDrop = function() {
		$('fieldset').each(function(f) {
			var total = $(this).find("div.drag").length;
			$(this).find("div.drag").each(function(ele) {
				var id = $(this).data("id");
				//console.log(id, total * 5);
				$('#' + id).val(total);
				total--;
			});
		})
	}
	$('#addcampaignupdate').on('click', function(e) {
		e.preventDefault();
		var wpneo_update_fields = $('#campaign_update_field').html();
		$('#campaign_update_addon_field').append(wpneo_update_fields);
		countRemovesBtn('.removecampaignupdate');
	});

	$('body').on('click', '.removecampaignupdate', function(e) {
		e.preventDefault();
		$(this).closest('.campaign_update_field_copy').html('');
		countRemovesBtn('.removecampaignupdate');
	});
	countRemovesBtn('.removecampaignupdate');

	function countRemovesBtn(btn) {
		var rewards_count = $(btn).length;
		if (rewards_count > 1) {
			$(btn).show();
		} else {
			$(btn).hide();
			if (btn == '.removeCampaignRewards') {
				$('.reward_group').show();
			}
			if (btn == '.removecampaignupdate') {
				$('#campaign_update_field').show();
			}
		}
		$(btn).first().hide();
	}
});