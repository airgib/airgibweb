<style>
#minhaconta-recebimento label {
	position: relative;
	display: block;
}
#contas li a.main {
	background: #fc0;
}
#contas, #contas li {
	list-style: none;
}
#contas li a {
	display: block;
	padding: 10px;
}
#contas li a:hover {
	background: #ccc;
}
</style>
<script>
jQuery(document).ready(function($){
	$('#contas li a.account').on('click',function(ev){
		$('#contas li a').removeClass('main');
		$(this).addClass('main');

		ev.preventDefault();
		return false;
	});
});
</script>
<form id="minhaconta-recebimento" class="grid">
	<div class="colu-1-2">
	 <h3> Adicionar conta para recebimento </h3>
		<label> Banco: <input type="text" name="nome" placeholder="Nome" id="nome"/>
	</label>
	<label> Tipo de Conta:
		<input type="text" name="sobrenome" placeholder="Sobrenome" id="sobrenome"/>
	</label>
	<label>
		Agência (com dígito):
		<input type="email" name="email" placeholder="Email" id="email"/>
	</label>
	<label>
		Conta Bancária:
		<input type="email" name="email" placeholder="Email" id="email"/>
	</label>
 	<label>
		Nome Titular:
		<input type="email" name="email" placeholder="Email" id="email"/>
	</label>
		<label>
		CPF do Titular:
		<input type="email" name="email" placeholder="Email" id="email"/>
	</label>
 
	<div class="clear clearfix" style="height:20px;"></div>
	<a href="#" class="btn button"> Adicionar conta<i class="fa fa-chevron-right"></i></a>
</div>
<div class="colu-1-2">
	<h3> Contas cadastradas </h3>
	<p> Selecione sua conta primária para recebimento </p>
	<ul id="contas">
	   <li><a class="account main" href="#" data-id="123"> Banco do Brasil - ***390</a> </li>
	   <li> <a href="#" class="account" data-id="123">Bradesco - ***390</a></li>
	   <li><a href="#"  class="account"  data-id="123"> Itaú - ***390</a></li>
	</ul>
	<a href="#" class="btn button"><i class="fa fa-save"></i> Salvar</a>
	<a href="#" class="btn button" style="float:right;background:red"><i class="fa fa-trash-o"></i> Excluir conta </a>

</div>
</form>