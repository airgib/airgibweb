<?php
global $current_user;
$first_name = get_user_meta($current_user->ID,'first_name',true);
$last_name = get_user_meta($current_user->ID,'last_name',true);
$description = get_user_meta($current_user->ID,'description',true);

$user = pods('user',$current_user->ID);
?>
<script>
jQuery(document).ready(function($){
	$('#minhaconta_salvar').on('click',function(ev){
		var _this = $(this);
		_this.html('<i class="fa fa-circle-o-notch fa-spin"</i>');
	 	//AIRWEB_API
	 	var data = $('#minhaconta_salvar').serializeJSON();
 
	 	$.ajax({
	        url: '<?php echo AIRWEB_API; ?>/userprofile/<?php echo $current_user->ID;?>',
	        type: 'PUT',
	        data: data,
	        success: function (data, status, xhr) {
	            console.log('data',data);
	            _this.html('Configurações salvas <i class="fa fa-chevron-right"></i>');
	            setTimeout(function(){  
	            	_this.html('Salvar Configurações <i class="fa fa-chevron-right"></i>'); 
            	}, 3000);
	        },
	        error: function (data, status, xhr) {

	        }
	    });
		ev.preventDefault();
		return false;
	});
});
</script>
<style>
#minhaconta-perfil label {
	position: relative;
	display: block;
}
</style>
<form id="minhaconta-perfil" class="grid">
	<div class="colu-1-2">
		<label> Nome: <input type="text" value="<?php echo $first_name;?>" name="nome" placeholder="Nome" id="first_name"/>
	</label>
	<label> Sobrenome:
		<input type="text" name="sobrenome" value="<?php echo $last_name;?>"  placeholder="Sobrenome" id="last_name"/>
	</label>
	<label>
		Email:
		<input type="email" name="email"  value="<?php echo $current_user->user_email;?>" placeholder="Email" id="user_email" readonly="readonly" readonly/>
	</label>
	<label>
	  Data de Nascimento:
  		<input type="text" name="birthdate" value="<?php echo $user->display('user_bday');?>"  placeholder="DD/MM/AAAA" id="user_bday"/>
	</label>
	<label>
	  Descreva seu perfil:
	  <textarea id="description" name="perfil"><?php echo $description;?></textarea>
	</label>
	<label>
		Localização: 
		<input type="text" name="current_city" value="<?php echo $user->display('current_city');?>" placeholder="Digite sua localização" id="current_city"/>
	</label>
	<div class="clear clearfix" style="height:20px;"></div>
	<a href="#" id="minhaconta_salvar" class="btn button"> Salvar Configurações <i class="fa fa-chevron-right"></i></a>
</div>
<div class="colu-1-2">
	 
</div>
</form>