<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
 
add_shortcode('minhaconta_perfil','minhaconta_perfil_f');
function minhaconta_perfil_f() {
	$html = '';
	ob_start();
		include AIRWEB_DIR_PATH.'/includes/templates/minhaconta-perfil.php';
		$html .= ob_get_contents();
	ob_end_clean();
	return $html;
}

add_shortcode('minhaconta_recebimento','minhaconta_recebimento_f');
function minhaconta_recebimento_f() {
	$html = '';
	ob_start();
		include AIRWEB_DIR_PATH.'/includes/templates/minhaconta-recebimento.php';
		$html .= ob_get_contents();
	ob_end_clean();
	return $html;
}
?>