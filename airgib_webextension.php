<?php
/*
 * Plugin Name:       Airgib Web Extension 
 * Plugin URI:        https://my.airgib.com
 * Description:       Airgib Web port/extension
 * Version:           2.0
 * Author:            Paulo Vieira
 * Author URI:        https://about.me/paulovieira
 * Text Domain:       airgib
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
define('AIRWEB_API', get_bloginfo('siteurl').'/wp-json/wp/v2');
define('AIRWEB_DIR_URL', plugin_dir_url(__FILE__));
define('AIRWEB_DIR_PATH', plugin_dir_path(__FILE__));
define('AIRWEB_PLUGIN_BASENAME', plugin_basename(__FILE__));

if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
}

if(!function_exists('execute')){
	function execute($query) {
		global $wpdb;
		$output =  $wpdb->get_results($query);
		return $output;
	}
}
add_action( 'wp_enqueue_scripts', 'airgib_webextension_scripts' );
function airgib_webextension_scripts() {
	wp_enqueue_script( 'jquery-ui-autocomplete' );

    wp_register_style( 'airgib_webextensioncss',plugins_url('assets/css/airgib_webextension.css',__FILE__ ));
    wp_enqueue_style('airgib_webextensioncss');


    wp_enqueue_script('airgib_webextensionjs', plugins_url( 'assets/js/airgib_webextension.js',__FILE__ ), array( 'jquery' ), false, true);
    wp_localize_script('airgib_webextensionjs', 'ajax_object', array( 'ajax_url' => admin_url('admin-ajax.php')));
    //https://github.com/marioizquierdo/jquery.serializeJSON
     wp_enqueue_script('airgib_serializeJson', plugins_url( 'assets/js/jquery.serializejson.min.js',__FILE__ ), array( 'jquery' ), false, true);

}


require_once 'includes/minhaconta.php';
require_once 'includes/meusespacos.php';

require_once 'includes/airgib.php';

 